#!/bin/sh

docker login registry.gitlab.com -u $GITLAB_USER -p $GITLAB_TOKEN
docker build -t registry.gitlab.com/walker-walks/openshift-minimal .
docker push registry.gitlab.com/walker-walks/openshift-minimal
