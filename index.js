var http = require("http");

http.createServer(function(request, response) {
  console.log('accessed - updated');
  response.writeHead(200, {"Content-Type": "text/plain"});
  response.write("Hello World - updated");
  response.end();
}).listen(8080);