FROM node:6-alpine
MAINTAINER Koji <stylethewalker@gmail.com>

EXPOSE 8080

WORKDIR /app
ADD ./index.js /app
ADD ./package.json /app

CMD ["npm", "start"]
